#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/default.mk
export SOURCE_DATE_EPOCH ?= $(shell dpkg-parsechangelog -SDate | date -f- +%s)

DESTDIR := $(CURDIR)/debian/tmp

# This is a subset of debhelper compat level 13 syntax.
SUBST := sed \
	-e 's![$$]{DEB_HOST_ARCH_BITS}!$(DEB_HOST_ARCH_BITS)!' \
	-e 's![$$]{DEB_HOST_MULTIARCH}!$(DEB_HOST_MULTIARCH)!' \
	$(NULL)

# This is a subset of dh-exec syntax.
ifeq ($(DEB_HOST_ARCH),amd64)
SUBST += -e 's/^\[amd64\] *//'
else
SUBST += -e '/^\[amd64\]/d'
endif
ifeq ($(DEB_HOST_ARCH),i386)
SUBST += -e 's/^\[i386\] *//'
else
SUBST += -e '/^\[i386\]/d'
endif

%:
	dh $@

# We open-code the Meson clean, configure, build, test, install steps
# because the debhelper in SteamRT 1 'scout' is too old to have built-in
# knowledge of Meson.

override_dh_auto_clean:
	rm -fr obj-$(DEB_HOST_GNU_TYPE)

override_dh_auto_configure:
	if ! meson obj-$(DEB_HOST_GNU_TYPE) \
		--sysconfdir=/etc \
		--prefix=/usr \
		--libexecdir=libexec \
		--libdir=lib/$(DEB_HOST_MULTIARCH) \
	; then \
		cat obj-$(DEB_HOST_GNU_TYPE)/meson-logs/meson-log.txt; \
		exit 1; \
	fi

override_dh_auto_build:
	ninja -v -C obj-$(DEB_HOST_GNU_TYPE)

override_dh_auto_test:
ifeq ($(filter nocheck,$(DEB_BUILD_OPTIONS)),)
	meson test -C obj-$(DEB_HOST_GNU_TYPE) --verbose
endif

override_dh_auto_install:
	DESTDIR=$(DESTDIR) ninja -C obj-$(DEB_HOST_GNU_TYPE) install

override_dh_install:
	set -e; for p in \
		steamrt-dev \
	; do \
		$(SUBST) < debian/$$p.install.in > debian/$$p.install; \
	done
	dh_install

override_dh_installdirs:
	set -e; for p in \
		steamrt-container-host-compat \
	; do \
		$(SUBST) < debian/$$p.dirs.in > debian/$$p.dirs; \
	done
	dh_installdirs

override_dh_link:
	set -e; for p in \
		steamrt-container-host-compat \
		steamrt-dev \
	; do \
		$(SUBST) < debian/$$p.links.in > debian/$$p.links; \
	done
	dh_link

override_dh_builddeb:
	find debian/steamrt-*/ -newermt @$(SOURCE_DATE_EPOCH) -print0 | xargs -0 touch -h -d @$(SOURCE_DATE_EPOCH)
	dh_builddeb

override_dh_gencontrol:
	python3 abi/generate-dependencies.py obj-$(DEB_HOST_GNU_TYPE)/abi/steam-runtime-abi.json
	dh_gencontrol \
		-plibfreetype-dev \
		-- \
		-v2.4.8-0steamrt$(DEB_VERSION_UPSTREAM) \
		${NULL}
	dh_gencontrol \
		-pautoconf2.69 \
		-- \
		-v2.69-0steamrt$(DEB_VERSION_UPSTREAM) \
		${NULL}
	dh_gencontrol \
		--remaining-packages \
		${NULL}

override_dh_missing:
	dh_missing --fail-missing
