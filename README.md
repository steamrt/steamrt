Steam Runtime 1 'scout'
=======================

scout is the traditional Steam Runtime, which has been used for the
Steam client and the majority of native Linux games since the initial
public release of Steam for Linux.

This has three uses:

* It's the basis for the traditional `LD_LIBRARY_PATH`-based Steam Runtime,
    `~/.steam/steam/ubuntu12_32/steam-runtime`, described here:
    <https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/ld-library-path-runtime.md>

* It provides additional libraries for the Steam Linux Runtime 1.0 (scout)
    container, described below, to provide compatibility
    for older games that were originally intended to run in the
    `LD_LIBRARY_PATH`-based runtime.

* It supplies the build environment and bundled libraries for the
    [pressure-vessel][] tools that start the various Steam Linux Runtime
    containers, both for scout-on-soldier and for the newer soldier
    and sniper runtimes. This is because we need the dependencies of
    `pressure-vessel` to be as old as possible, so that they work on as
    many systems as possible.

Quick reference links:

* [General information about the `LD_LIBRARY_PATH` runtimes][ldlp]
* [General information about the container runtimes][container runtime]
* Release notes: [scout library stack][Release notes],
    [Steam Linux Runtime 1.0 (scout) container][SLR release notes]
* [SDK][]
* [Guide for game developers][]

Requirements
------------

The various Steam Runtime components share several
[assumptions about the host distribution][distro assumptions] with
the Steam client.

How this fits into the overall Steam Runtime project
----------------------------------------------------

1. Build .deb packages for the content of the Steam Runtime.
    `steamrt` is one of these packages.

2. Put together the .deb packages into the `LD_LIBRARY_PATH`-based
    Steam Runtime.
    This step is done by the scripts in [steam-runtime][],
    but the choice of the actual packages to include is mostly delegated to
    `steamrt`.

3. Also put together the .deb packages into a Flatpak-style container runtime,
    used for the Platform and SDK Docker images.
    This step is done by [flatdeb-steam][],
    but the choice of the actual packages to include is mostly delegated to
    `steamrt`.

The Steam Linux Runtime 1.0 (scout) container
---------------------------------------------

The *Steam Linux Runtime 1.0 (scout)* compatibility tool allows games
that were compiled
for the Steam Runtime 1 'scout' environment to be run in a container,
providing them with a predictable library stack that minimizes changes and
incompatibilities as the host OS is upgraded.
It is a separate Steam depot, like games or Proton.
The third-party SteamDB site tracks it as
<https://steamdb.info/app/1070560/>.

The Steam Deck automatically uses the Steam Linux Runtime 1.0 (scout)
container for many native Linux games.
Some games also default to being run in this container on Linux desktop
systems, for example [Dota 2][]
between [its January 2022 update][Dota 2 scout SLR] and mid 2023.
This mechanism is not currently available for third-party games on
desktop Linux, but users can opt-in to running specific games in the
*Steam Linux Runtime 1.0 (scout)* container via *Properties* → *Compatibility*.

The compatibility tool is installed automatically whenever it is selected
to run a game, and can also be installed by running this command:

    steam steam://install/1070560

Since July 2021, the *Steam Linux Runtime 1.0 (scout)* compatibility tool
consists of a
[Steam Runtime 2 'soldier'][soldier]
[container][container runtime],
with the Steam Runtime 1 'scout'
[`LD_LIBRARY_PATH`-based runtime][ldlp]
overlaid onto it as an additional layer.
This design is described in more detail here:
<https://github.com/ValveSoftware/steam-runtime/blob/master/doc/possible-designs.md#2018-ld_library_path-scout-runtime--newer-platform--scout-again>.

<details><summary>Historical information</summary>

Older versions of the *Steam Linux Runtime 1.0 (scout)* compatibility tool
used a [container runtime][]
with purely Steam Runtime 1 'scout' libraries, originally announced in
"[Steam for Linux client adds support for Linux namespaces][]"
and described in more detail here:
<https://github.com/ValveSoftware/steam-runtime/blob/master/doc/possible-designs.md#2018-ld_library_path-scout-runtime--2019-pressure-vessel-scout-platform>.
However, this turned out to have poor game compatibility in practice,
because many games were not compiled in a "pure" scout environment and
require newer library versions than what scout provides.
In July 2021 it was replaced by the design described above.

</details>

## <span id="release-notes">Release notes</span>

[Release notes for scout updates are available][Release notes].
The newest entries in these release notes will often describe beta
releases that are only available via the [Steam Client beta][], or
sometimes planned releases that are not yet available to the public.

The "Steam Linux Runtime 1.0 (scout)" environment has
[its own release notes][SLR release notes],
and is also affected by
[changes to the "Steam Linux Runtime 2.0 (soldier)" container][soldier release notes].

Where can I get it?
-------------------

### `LD_LIBRARY_PATH` runtime

The `LD_LIBRARY_PATH`-based Steam Runtime is automatically downloaded as
part of Steam.
Because it is built into Steam, beta versions can be selected by
opting in to the [Steam Client beta][].

### Steam Linux Runtime 1.0 (scout) container

The Steam Linux Runtime 1.0 (scout) container, app ID 1070560,
will be downloaded
automatically when you install or upgrade a game that requires it.
It will be downloaded to your Steam library as `common/SteamLinuxRuntime`.
This container requires the "Steam Linux Runtime 2.0 (soldier)" compatibility
tool, which will be downloaded automatically as
`common/SteamLinuxRuntime_soldier`.

To run other games in this container, select the
"Steam Linux Runtime 1.0 (scout)" compatibility tool in the game's properties.
If that name is not available, look for "Steam Linux Runtime" instead.
It will be downloaded automatically if not already present.
This option is only available for native Linux games that were compiled
against Steam Runtime 1 'scout'.

A beta version of the "Steam Linux Runtime 1.0 (scout)" compatibility tool
can be selected by locating the "Steam Linux Runtime 1.0 (scout)"
compatibility tool in your Steam library, opening the Properties dialog,
and choosing a beta branch in the Betas tab.
Beta versions of "Steam Linux Runtime 2.0 (soldier)" can be selected in a
similar way.
Please report any regressions to the
[Steam Runtime issue tracker][]
so that they can be fixed.

Before September 2023, the displayed name for this tool was
"Steam Linux Runtime", which might still appear in some contexts.

### Additional files

<https://repo.steampowered.com/steamrt1/images/> contains
several versions of scout. Each version's subdirectory contains:

* source code in the `sources` subdirectory
* detached debug symbols in the `dbgsym` subdirectory
* "Platform" container images that can run games
  (`*.Platform-*-runtime.tar.gz`, which can also be converted into Flatpak
  runtimes)
* "SDK" container images that can run games with debugging tools available
  (`*.Sdk-*-runtime.tar.gz` and `*.Sdk-*-debug.tar.gz`,
  which can also be converted into Flatpak runtimes)
* "Platform" container images for testing and QA
  (`*.Platform-*-sysroot.tar.gz`, and `*.Platform-*-sysroot.Dockerfile`,
  which are published as a Docker image via the
  [steamrt/scout/platform Gitlab project][Platform])
* "SDK" container images for compiling your own software
  (`*.Sdk-*-sysroot.tar.gz`, and `*.Sdk-*-sysroot.Dockerfile`,
  which are published as a Docker image via the
  [steamrt/scout/sdk Gitlab project][SDK])

What's in this repository?
--------------------------

This repository mostly provides "metapackages". This is Debian jargon for
a package that is (almost) empty, and pulls in desired packages by having
dependencies on them.

In the Steam Runtime, we are only interested in `amd64` (Debian's name for
64-bit PCs, also known as `x86_64`) and `i386` (Debian's name for 32-bit
PCs, also known as `i586`, `i686` or IA-32).

The traditional `LD_LIBRARY_PATH` runtime consists of `amd64` and `i386`
versions of:

* `steamrt-libs`
* `steamrt-legacy`

and their dependencies.

Similarly, the Platform container consists of:

* `steamrt-container`
* `steamrt-container-host-compat`
* `steamrt-libs`

The SDK container used for debugging and compilation is based on the
Platform, and adds:

* `steamrt-dev`
* `steamrt-libdevel`
* `steamrt-libdevel-non-multiarch`
* `steamrt-libdebug`
* `steamrt-libdebug-non-multiarch`

The i386 containers only include i386 packages. The `LD_LIBRARY_PATH`
runtime includes both amd64 and i386.

The Platform and SDK containers are built in `amd64,i386` and
`i386` versions, where the `amd64,i386` runtime contains both,
and the `i386` runtime is purely 32-bit.

Most packages are installed for both word sizes in parallel,
except for `steamrt-container`, `steamrt-dev` and `*-non-multiarch`,
which are only installed for the primary architecture (so the
`amd64,i386` container installs those for `amd64`).

Developing software that runs in scout
--------------------------------------

A SDK environment is available as an OCI image, suitable for use with
OCI-compatible container tools such as Docker, Podman and Toolbx.
Please see [the SDK project][SDK] for more details.

A [guide for game developers][] provides more information on how to debug
processes in the Steam Linux Runtime 1.0 (scout) environment.

Backporting policy
------------------

### What we can backport into scout

In general, we can backport application-level libraries like Pipewire
and SDL, subject to some conditions:

  * The library needs to have a proper SONAME with a stable ABI,
    for example `libpipewire-0.3.so.0`.
  * There needs to be a minor ABI version number in the library's
    physical filename, which goes up with each release.
    For example, `libpipewire-0.3.so.0 -> libpipewire-0.3.so.0.339.0`
    is suitable: each new release increases the minor ABI version.
    However, `libdrm.so.2 -> libdrm.so.2.4.0` is not suitable (see below).
  * If the new library has runtime dependencies which aren't satisfied
    by the Steam Runtime, we need to backport those first, or patch the
    library to avoid the dependencies.
  * If the new library has build-time dependencies which aren't satisfied
    by the Steam Runtime, we need to backport those first, or patch the
    build system to avoid the dependencies.
  * If the new library loads functionally-necessary plugins, data or
    similar, we need to patch it to respect the `$STEAM_RUNTIME` environment
    variable.

We can backport invididual bug-fixes without backporting an entire new
upstream version, but Steam and games cannot rely on having those bug-fixes
at runtime, because our library might have been overridden by a newer
upstream version from the host system that does not have those bug-fixes.

Similarly, we can backport invididual features without backporting an
entire new upstream version, but Steam and games cannot rely on having those
features at runtime, because our library might have been overridden by a
newer upstream version from the host system that does not have those
features. In particular, if we backport something that adds new ABI,
we usually should not add new symbols to public header files, to force
Steam and games to use forwards-compatible mechanisms like `dlsym()`
to find new ABIs.

### Preferred versions to backport

If we can, we prefer to backport the version that was included in a
stable release of Debian or an LTS release of Ubuntu, in preference to
using an intermediate version. This gives us a source for security and
bugfix updates.

If we need a version newer than what's in the current Debian stable release,
we prefer to use the version from Debian testing or unstable, but in this
case we should plan to track its updates until the next stable release.

If we need a version newer than what's in Debian unstable, we can go to
experimental or the latest upstream version, but we need to be careful to
track newer releases in this case.

### What we can't backport

We cannot backport glibc: we have to use the system version of
glibc, because the ELF interpreter `ld.so(8)` needs to be the same
version as `libdl.so.2`, which in turn needs to be the same version as
`libc.so.6`. The absolute path to `ld.so(8)` is hard-coded into every
executable, and cannot be a relative path.

We cannot backport libraries that are closely related to the graphics
stack, notably the libraries built by Mesa, including `libgbm.so.1`. The
Steam Runtime is designed to use the version of Mesa from the host
system, to maximize the probability that it will work with the user's
kernel and hardware.

We cannot safely backport libraries where the minor ABI version in
the physical filename does not increase. For example,
`libdrm.so.2 -> libdrm.so.2.4.0` stays the same across multiple releases,
even while adding new ABI. This means we cannot tell whether our bundled
version is older or newer than the host version, which means we cannot
guarantee that games will be using a version that is the same as or
newer than our backport.

Similarly, we cannot safely backport libraries where no minor ABI version
is present in the physical filename at all (such as `libnss3.so`).

In general, we cannot safely backport libraries that have undergone
non-backwards-compatible changes while keeping the same SONAME.

We generally should not backport bug-fixes and new features that have
not been included in upstream's version control system, and preferably
included in a formal release as well.

<!-- References: -->

[Dota 2 scout SLR]: https://store.steampowered.com/news/app/570/view/4978168332488878344
[Dota 2]: https://store.steampowered.com/app/570/Dota_2/
[Guide for game developers]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/slr-for-game-developers.md
[Platform]: https://gitlab.steamos.cloud/steamrt/scout/platform
[Release notes]: https://gitlab.steamos.cloud/steamrt/steamrt/-/wikis/Scout-release-notes
[soldier release notes]: https://gitlab.steamos.cloud/steamrt/steamrt/-/wikis/Soldier-release-notes
[SLR release notes]: https://gitlab.steamos.cloud/steamrt/steamrt/-/wikis/Steam-Linux-Runtime-1.0-(scout)-release-notes
[SDK]: https://gitlab.steamos.cloud/steamrt/scout/sdk
[Steam Client beta]: https://developer.valvesoftware.com/wiki/Category:Beta_Releases
[Steam Runtime issue tracker]: https://github.com/ValveSoftware/steam-runtime/issues
[Steam for Linux client adds support for Linux namespaces]: https://steamcommunity.com/app/221410/discussions/0/1638675549018366706/
[container runtime]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/container-runtime.md
[distro assumptions]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/distro-assumptions.md
[flatdeb-steam]: https://gitlab.steamos.cloud/steamrt/flatdeb-steam
[ldlp]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/ld-library-path-runtime.md
[pressure-vessel]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/pressure-vessel.md
[scout]: https://gitlab.steamos.cloud/steamrt/steamrt/-/blob/steamrt/scout/README.md
[soldier]: https://gitlab.steamos.cloud/steamrt/steamrt/-/blob/steamrt/soldier/README.md
[steam-runtime]: https://github.com/ValveSoftware/steam-runtime
[steam-runtime-tools]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools
