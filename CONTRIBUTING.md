Contributing to steamrt
=======================

Issue tracking
--------------

Our main bug tracking system for the whole Steam Runtime is
<https://github.com/ValveSoftware/steam-runtime/issues>. Before reporting
an issue, please take a look at the
[bug reporting information](https://github.com/ValveSoftware/steam-runtime/blob/HEAD/doc/reporting-steamlinuxruntime-bugs.md)
to make sure your issue report contains all the information we need.

The issue tracker on our Gitlab installation, `gitlab.steamos.cloud`, is
primarily used internally by Steam Runtime developers to track issues for
which we already know the technical details of what is happening.

Contributing code
-----------------

At the moment our Gitlab installation, `gitlab.steamos.cloud`, is not
set up to receive merge requests from third-party contributors. However,
git is a distributed version control system, so it is possible to push a
clone of the steamrt git repository to some other git hosting platform
(such as Github or `gitlab.com`) and send a link to a proposed branch
via an issue report.

If you want to contribute code to steamrt, please include a
`Signed-off-by` message in your commits to indicate acceptance of the
[Developer's Certificate of Origin](https://developercertificate.org/)
terms.

Common tasks
============

Adding a library to the SDK
---------------------------

Suppose you want to add `libfoo-dev` to the SDK.

Install `libfoo-dev` in a `scout` container/chroot, and check what other
packages it pulls in, and where they came from (`scout` or `precise`).
Check that those packages have acceptable licensing terms.

If any of the new packages came from `precise`, import those source
packages from precise into OBS so that we get our own replacement binary
packages built.

Check whether you can install `libfoo-dev:amd64` and `libfoo-dev:i386`
in a `scout` container/chroot without needing to remove anything else.

Edit `debian/control` and add `libfoo-dev` to an appropriate `Depends`
list. If you can co-install the amd64 and i386 versions of it, add it
to `steamrt-libdevel`. If not, add it to `steamrt-libdevel-non-multiarch`.

If you want to be able to ship games that use this library unconditionally,
you'll need to provide the library in the runtime (see below).

Adding a library to the Platform
--------------------------------

Suppose you want to add `libFoo.so.1` to the runtime.

Install `libfoo1` (or whatever is the right package name) in a `scout`
container/chroot, and check what other packages it pulls in, and where
they came from (`scout` or `precise`).  Check that those packages have
acceptable licensing terms.

If any of the new packages came from `precise`, import those source
packages from precise into OBS so that we get our own replacement binary
packages built.

Check whether you can install `libfoo1:amd64` and `libfoo1:i386`
in a `scout` container/chroot without needing to remove anything else.
If you can't, improve the packaging until you can (install to Multi-Arch
directories and set `Multi-Arch: same`). We don't support libraries that
are not multi-arch co-installable.

Edit `abi/steam-runtime-abi.yaml` and add the new library and each of its
(recursive) dependencies. If the name of the package matches the SONAME,
you can just add it to the list as a string, similar to `libX11.so.6`. If
not, you'll have to add it as a dict that contains a `deb:` key, similar
to `libz.so.1` containing `deb: zlib1g`.

In most cases, the library and all its recursive dependencies will need
to be added to the runtime's ABI. In the rare situation where we want
to take a dependency from the host system, like glibc and Mesa, you'll
also need to edit `build-runtime.py` in the `steam-runtime` git repo to
make that work.

After editing `abi/steam-runtime-abi.yaml`, regenerate
the `abi/expectations` directory by running
`abi/maintainer-generate-expectations.py` in a `scout` chroot or container.

If you want to be able to compile against this library without downloading
extra packages, you'll need to provide the library in the SDK (see above).

Testing a new scout runtime
---------------------------

Build the runtime using CI or the `build-runtime.py` script.
Various artifacts are produced, but the one we need here is
`steam-runtime.tar.xz`.

Unpack that tarball to get a `steam-runtime` directory in a temporary
location, for example `~/tmp/steam-runtime`, so that you have files
that include `~/tmp/steam-runtime/run.sh`.

Then run Steam with the `STEAM_RUNTIME` and `STEAM_RUNTIME_SCOUT`
environment variables set to the absolute path to the scout runtime,
for example:

    $ export STEAM_RUNTIME="$HOME/tmp/steam-runtime"
    $ export STEAM_RUNTIME_SCOUT="$STEAM_RUNTIME"
    $ steam

`STEAM_RUNTIME` is used for Steam itself, for native Linux games
with no special compatibility tool configured, and for old versions of
Proton.

`STEAM_RUNTIME_SCOUT` is used for native Linux games that run under the
`Steam Linux Runtime 1.0 (scout)` compatibility tool.

It can be useful to set the Launch Options of each test game to:

```
steam-runtime-launch-options -- %command%
```

so that you will be given the choice between different versions of the
`LD_LIBRARY_PATH` runtime, and different versions of
`Steam Linux Runtime 1.0 (scout)`, every time you launch the game -
this is often more convenient than going into the game's Properties
every time you want to switch between runtimes.

Alternatively, you can overwrite the following files in
`~/.steam/root/ubuntu12_32/` with the versions from the new runtime,
then run Steam with the unsupported parameter that makes it skip the
file integrity check that would normally re-download them (deliberately
not documented here):

* `steam-runtime.checksum` (rename from `steam-runtime.tar.xz.checksum`)
* `steam-runtime.tar.xz`
* `steam-runtime.tar.xz.part0` (a copy of `steam-runtime.tar.xz`)
* `steam-runtime.tar.xz.part1` (truncate to 0 bytes)
* `steam-runtime.tar.xz.part2` (truncate to 0 bytes)

To test without using Steam, you must run the `setup.sh` script at least
once, and then wrap programs in the `run.sh` script, for example:

    $ cd .../"Team Fortress 2"
    $ ~/tmp/steam-runtime/setup.sh
    $ ~/tmp/steam-runtime/run.sh -- ./hl2.sh

Testing a new scout SDK
-----------------------

Build the runtime using CI or `flatdeb-steam`. Various artifacts are
produced, but the ones we'll need here are
`com.valvesoftware.SteamRuntime.Sdk-amd64,i386-scout-sysroot.tar.gz`
and `com.valvesoftware.SteamRuntime.Sdk-amd64,i386-scout-sysroot.Dockerfile`.
Download them into a new, empty directory and `cd` into that directory.

For Docker users:

    $ sudo docker build -t steamrt_scout_amd64:latest -f *.Dockerfile .

Then you can use the `steamrt_scout_amd64` Docker image for builds.

For Podman users: it's the same as Docker, but use `podman build` instead
of `sudo docker build`.

For `schroot` users:

    $ /path/to/steam-runtime/setup_chroot.sh --amd64 --tarball *-sysroot.tar.gz

The `amd64,i386` image only provides selected packages for 32-bit use.
If you are building i386 software that requires broader 32-bit support,
replace `amd64,i386` with `i386`, replace `steamrt_scout_amd64` with
`steamrt_scout_i386`, and replace `--amd64` with `--i386`.

Committing a new version to Steam
---------------------------------

Please see the release checklist or Valve-internal documentation for details.
