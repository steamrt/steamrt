#!/usr/bin/env python3
# Copyright 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

import logging
import os
import subprocess
import sys

from steamrt_abi import AbiData


logger = logging.getLogger('have-all-libraries')


def main() -> None:
    abi = AbiData(
        os.path.join(
            os.path.dirname(__file__),
            'steam-runtime-abi.yaml',
        )
    )
    failed = False

    multiarchs = sys.argv[1:] or [subprocess.check_output([
        'dpkg-architecture', '-qDEB_HOST_MULTIARCH',
    ], universal_newlines=True).strip('\n')]

    for multiarch in multiarchs:
        for lib in abi.shared_libraries.values():
            for libdir in (
                os.path.join('/lib', multiarch),
                os.path.join('/usr/lib', multiarch),
            ):
                if os.path.exists(os.path.join(libdir, lib.soname)):
                    break
            else:
                if not lib.from_host_system:
                    logger.error('%s/%s not found', multiarch, lib.soname)
                    failed = True

            for alias in lib.aliases:
                for libdir in (
                    os.path.join('/lib', multiarch),
                    os.path.join('/usr/lib', multiarch),
                ):
                    if os.path.exists(os.path.join(libdir, alias)):
                        break
                else:
                    logger.error(
                        '%s/%s not found (should be symlink to %s)',
                        multiarch, alias, lib.soname,
                    )
                    failed = True

    if failed:
        sys.exit(1)


if __name__ == '__main__':
    logging.basicConfig()
    main()
