#!/usr/bin/env python3
# Copyright 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

import sys

try:
    from typing import (
        Set,
    )
    # placate pyflakes
    Set
except ImportError:
    pass

from steamrt_abi import AbiData


def main() -> None:
    abi = AbiData(sys.argv[1])

    debs = set()    # type: Set[str]

    for lib in abi.shared_libraries.values():
        if lib.generate_dependency:
            debs.add(lib.deb)

    for deb in abi.data['extra_debs']['libs']:
        debs.add(deb)

    with open('debian/steamrt-libs.substvars', 'a') as writer:
        writer.write(
            'steamrt:Libs={}\n'.format(
                ', '.join(sorted(debs)),
            )
        )


if __name__ == '__main__':
    main()
