#!/usr/bin/env python3
# Copyright 2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

import sys

from steamrt_abi import AbiData


def main() -> None:
    abi = AbiData(sys.argv[1])

    output_file = sys.argv[2]

    with open(output_file, 'w') as writer:
        for soname in sorted(abi.shared_libraries.keys()):
            lib = abi.shared_libraries[soname]

            if (
                lib.compare_by
                or lib.public_symbol_versions
                or lib.public_symbols
            ):
                writer.write('[Library {}]\n'.format(lib.soname))
                if lib.compare_by:
                    writer.write(
                        'CompareBy={};\n'.format(
                            ';'.join(lib.compare_by)
                        )
                    )

                if lib.public_symbol_versions:
                    if lib.public_symbol_versions[-1] not in ('*', '!*'):
                        raise SystemExit(
                            'last item of public_symbol_versions for %s '
                            'should be * or !*', lib.soname
                        )
                    writer.write(
                        'PublicSymbolVersions={};\n'.format(
                            ';'.join(lib.public_symbol_versions)
                        )
                    )

                if lib.public_symbols:
                    if lib.public_symbols[-1] not in ('*', '!*'):
                        raise SystemExit(
                            'last item of public_symbols for %s '
                            'should be * or !*', lib.soname
                        )
                    writer.write(
                        'PublicSymbols={};\n'.format(
                            ';'.join(lib.public_symbols)
                        )
                    )


if __name__ == '__main__':
    main()
