#!/usr/bin/env python3
# Copyright 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

import json
import os
import sys

import yaml


if __name__ == '__main__':
    with open(sys.argv[1], 'r') as reader:
        data = yaml.safe_load(reader)

    # Chosen to sort before all the real keys
    data['  GENERATED FILE, DO NOT EDIT'] = True

    with open(sys.argv[2] + '.new', 'w') as writer:
        json.dump(data, writer, indent=1, sort_keys=True)
        writer.write('\n')

    os.rename(sys.argv[2] + '.new', sys.argv[2])
