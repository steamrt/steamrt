#!/bin/sh
# Copyright © 2018-2023 Collabora Ltd
# SPDX-License-Identifier: MIT

set -eu

if [ -z "${G_TEST_SRCDIR-}" ]; then
    me="$(readlink -f "$0")"
    G_TEST_SRCDIR="${me%/*}"
fi

cd "$G_TEST_SRCDIR/.."
echo "TAP version 13"

n=0
for shell_script in \
        tests/*.sh \
        tests/platform/*.t \
        tests/sdk/*.t \
        ; do
    n=$((n + 1))
    if bash -n "$shell_script" >&2; then
        echo "ok $n - $shell_script"
    elif [ -n "${LINT_WARNINGS_ARE_ERRORS-}" ]; then
        echo "not ok $n - $shell_script"
    else
        echo "not ok $n # TO""DO - $shell_script"
    fi
done

echo "1..$n"
