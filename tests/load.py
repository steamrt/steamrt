#!/usr/bin/env python3
# Copyright 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

import os
import sys


def main() -> None:
    sys.path[:0] = [os.path.join(os.path.dirname(__file__), os.pardir, 'abi')]
    from steamrt_abi import AbiData

    if 'G_TEST_BUILDDIR' in os.environ:
        filename = os.path.join(
            os.environ['G_TEST_BUILDDIR'],
            os.pardir,
            'abi',
            'steam-runtime-abi.json',
        )
    else:
        filename = ''

    # Just check that we can load it
    AbiData(filename)


if __name__ == '__main__':
    print('1..1')
    main()
    print('ok 1')
