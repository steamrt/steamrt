#!/bin/bash
# Copyright © 2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -eux
set -o pipefail

isdir () {
    ( ls -ald "$1" || true ) | sed -e 's/^/# /'
    test -d "$1"
}

exists () {
    ( ls -ald "$1" || true ) | sed -e 's/^/# /'
    test -e "$1"
}

# Assert that compatibility mount points etc. are available for
# pressure-vessel.

echo "1..1"

# shellcheck disable=SC1091
. /etc/os-release

case "${VARIANT_ID-}" in
    (com.valvesoftware.steamruntime.platform-*)
        is_platform=yes
        ;;

    (*)
        is_platform=
        ;;
esac

# Platform runtimes don't have dpkg-query, so we can't entirely rely on it
if command -v dpkg-query >/dev/null; then
    dpkg-query -W -f '${Package}\t${Version}\t${Status}\n' \
        steamrt-container-host-compat | sed -e 's/^/# /'

    case "$(dpkg-query -W -f '${Status}' steamrt-container-host-compat:amd64)" in
        (*\ installed)
            is_amd64=yes
            ;;
        (*)
            is_amd64=
            ;;
    esac

    case "$(dpkg-query -W -f '${Status}' steamrt-container-host-compat:i386)" in
        (*\ installed)
            is_i386=yes
            ;;
        (*)
            is_i386=
            ;;
    esac
else
    case "${VARIANT_ID-}" in
        (com.valvesoftware.steamruntime.platform-amd64_i386-*)
            is_amd64=yes
            if [ "${VERSION_CODENAME-}" = heavy ]; then
                # heavy amd64,i386 Platform is actually just amd64;
                # only the SDK has i386 support
                is_i386=
            else
                is_i386=yes
            fi
            ;;
        (com.valvesoftware.steamruntime.platform-amd64-*)
            is_amd64=yes
            is_i386=
            ;;
        (com.valvesoftware.steamruntime.platform-i386-*)
            is_amd64=
            is_i386=yes
            ;;
        (*)
            is_amd64=
            is_i386=
            ;;
    esac
fi

isdir /usr/lib/gconv
isdir /usr/lib/locale
isdir /usr/share/i18n
isdir /usr/share/libdrm
exists /usr/bin/pulseaudio

if [ -n "$is_amd64" ]; then
    isdir /usr/lib64
    isdir /usr/lib64/gconv
    isdir /usr/lib64/locale
    isdir /usr/lib/x86_64-linux-gnu/gconv
    exists /lib64/ld-linux-x86-64.so.2
    exists /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2
    if [ -n "$is_platform" ]; then
        exists /usr/lib/ld-linux-x86-64.so.2
        exists /usr/x86_64-pc-linux-gnu/lib/ld-linux-x86-64.so.2
    fi
fi

if [ -n "$is_i386" ]; then
    isdir /usr/lib32
    isdir /usr/lib32/gconv
    isdir /usr/lib32/locale
    isdir /usr/lib/i386-linux-gnu/gconv
    exists /lib/ld-linux.so.2
    exists /lib/i386-linux-gnu/ld-linux.so.2
    if [ -n "$is_platform" ]; then
        exists /usr/i686-pc-linux-gnu/lib/ld-linux.so.2
    fi
fi

echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
