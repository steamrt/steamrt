#!/bin/bash
# Copyright © 2019-2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

echo "1..1"

# shellcheck disable=SC1091
. /etc/os-release

# In the case of the Platform the python3 symlink is set up by the
# container build, not by the packages we install, so we cannot
# rely on it existing in arbitrary minimal chroots.
case "${VARIANT_ID-}" in
    (com.valvesoftware.steamruntime.platform-* | com.valvesoftware.steamruntime.sdk-*)
        python3 -c 'print("ok 1")'
        ;;
    (*)
        echo 'ok 1 # SKIP not a Platform or SDK container'
        ;;
esac

# vim:set sw=4 sts=4 et ft=sh:
