#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x
set -o pipefail

exec 3>&1 1>&2

echo "1..1" >&3

test -f /usr/lib/steamrt/expectations/i386-linux-gnu/libc6.symbols
test -f /usr/lib/steamrt/expectations/x86_64-linux-gnu/libc6.symbols
test -f /usr/lib/steamrt/steam-runtime-abi.json

steam-runtime-system-info | tee "${AUTOPKGTEST_ARTIFACTS:-.}"/system-info.json

echo "ok 1" >&3
