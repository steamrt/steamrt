#!/bin/bash
# Copyright © 2019-2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple project using gcc 9.

echo "1..1"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

cp -a examples/make-glib/* "$tmpdir"

make -C "$tmpdir" V=1 CC=gcc-9
"$tmpdir/hello"
make -C "$tmpdir" V=1 CC=gcc-9 clean

echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
