#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple project using Autotools.

echo "1..1"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

cp -a examples/autotools "$tmpdir/srcdir"

cd "$tmpdir/srcdir"
export ACLOCAL=aclocal-1.15
export AUTOMAKE=automake-1.15
NOCONFIGURE=1 ./autogen.sh

mkdir "$tmpdir/builddir"
cd "$tmpdir/builddir"
../srcdir/configure

make -C "$tmpdir/builddir" V=1
"$tmpdir/builddir/hello"
make -C "$tmpdir/builddir" V=1 clean

echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
