#!/bin/bash
# Copyright © 2019-2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple project using meson and ninja.

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

# Meson wants a real locale
export LC_ALL=C.UTF-8

n=0
failed=

for compiler in \
    '' \
    binutils-2.35 \
    gcc \
    gcc-4.6 \
    gcc-4.8 \
    gcc-5 \
    gcc-9 \
    clang \
    clang-3.4 \
    clang-3.6 \
    clang-3.8 \
; do
    n=$((n + 1))

    case "$compiler" in
        ('' | gcc | clang)
            # default version, guaranteed to be present in SDK
            ;;

        (gcc-4.6 | gcc-4.8 | gcc-5 | gcc-9 | clang-3.4 | clang-3.6 | clang-3.8)
            # explicitly included in SDK
            ;;

        (binutils-2.35)
            # explicitly included in SDK
            ;;

        (*)
            if ! command -v "$compiler" >/dev/null; then
                echo "ok $n # SKIP $compiler not in PATH"
                continue
            fi
            ;;
    esac

    if \
        meson setup \
            ${compiler:+--native-file "${compiler}.txt"} \
            "$tmpdir/b" \
            "$(pwd)/examples/meson-glib" \
        && ninja -v -C "$tmpdir/b" \
        && "$tmpdir/b/hello" \
        && ninja -v -C "$tmpdir/b" clean \
    ; then
        echo "ok $n - ${compiler:-default}"
    else
        echo "not ok $n - ${compiler:-default}"
        failed=1
    fi

    rm -fr "$tmpdir/b"
done

echo "1..$n"

if [ -n "$failed" ]; then
    exit 1
fi

# vim:set sw=4 sts=4 et ft=sh:
