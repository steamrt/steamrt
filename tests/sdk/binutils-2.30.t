#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can select binutils-2.30 explicitly.

echo "1..1"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

cp -a examples/make-glib/* "$tmpdir"

echo "It's OK for this first one to fail with the default ld" >&2
e=0
make -C "$tmpdir" V=1 CFLAGS=-Wl,--require-defined=printf || e=$?
echo "exit status $e" >&2
make -C "$tmpdir" V=1 clean

export PATH="/usr/lib/binutils-2.30/bin:$PATH"

make -C "$tmpdir" V=1 CFLAGS=-Wl,--require-defined=printf
"$tmpdir/hello"
make -C "$tmpdir" V=1 clean

echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
