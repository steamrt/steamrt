#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple project using make and debug it
# using gdb.

echo "1..2"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

gdb="$(command -v gdb)"
echo "ok 1 - gdb exists at $gdb"

if [ -e /.dockerenv ] || grep docker /proc/self/cgroup >/dev/null; then
    echo "ok 2 # SKIP - we cannot usually ptrace inside Docker"
    exit 0
fi

cp -a examples/make-sdl/* "$tmpdir"

make -C "$tmpdir" V=1
gdb --batch --return-child-result -x tests/sdk/gdb-script --args "$tmpdir/hello"
make -C "$tmpdir" V=1 clean

echo "ok 2 - could use gdb"

# vim:set sw=4 sts=4 et ft=sh:
