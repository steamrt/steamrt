#!/bin/bash
# Copyright © 2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x
set -o pipefail

# Test that xvfb-run works.

echo "1..2"

xr="$(command -v xvfb-run)"
echo "ok 1 - xvfb-run exists at $xr"

xvfb-run -a -e /dev/stderr -s '-screen 0 1280x1024x24' xdpyinfo 2>&1 \
    | sed -e 's/^/# /'

echo "ok 2 - could run xvfb-run wrapping xdpyinfo"

# vim:set sw=4 sts=4 et ft=sh:
