#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple project using CMake.

echo "1..1"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

srcdir="$(pwd)"
cd "$tmpdir"
cmake "$srcdir/examples/cmake-glib"
cd "$srcdir"
make -C "$tmpdir" VERBOSE=1
"$tmpdir/hello"
make -C "$tmpdir" VERBOSE=1 clean

echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
