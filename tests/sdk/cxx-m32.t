#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple C project using make and the
# -m32 compiler option.

case "$(dpkg --print-architecture)" in
    (amd64|i386)
        ;;
    (*)
        echo "1..0 # SKIP - not x86"
        exit 0
        ;;
esac

# shellcheck disable=SC1091
. /etc/os-release

# The default gcc version is set to 4.8 by a flatdeb post_script, not by
# any package we install, so we cannot rely on it in arbitrary minimal
# chroots.
case "${VARIANT_ID-}" in
    (com.valvesoftware.steamruntime.sdk-*)
        ;;
    (*)
        echo '1..0 # SKIP - not a SDK container'
        exit 0
        ;;
esac

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

cp -a examples/cxx/* "$tmpdir"

failed=0
test_num=0

for cc_cxx in gcc/g++ clang/clang++; do
    test_num=$(( test_num + 1 ))
    CC="${cc_cxx%%/*} -m32"
    CXX="${cc_cxx##*/} -m32"
    echo "# CC=\"$CC\" CXX=\"$CXX\""

    make -C "$tmpdir" V=1 CC="$CC" CXX="$CXX"
    file "$tmpdir/hello"
    grep -F /lib/ld-linux.so.2 "$tmpdir/hello"
    "$tmpdir/hello"
    make -C "$tmpdir" V=1 CC="$CC" CXX="$CXX" clean

    echo "ok $test_num - $cc_cxx"
done

echo "1..${test_num}"
exit "$failed"

# vim:set sw=4 sts=4 et ft=sh:
