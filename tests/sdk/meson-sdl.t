#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple project using meson and ninja.

echo "1..1"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

# Meson wants a real locale
export LC_ALL=C.UTF-8

meson "$tmpdir" "$(pwd)/examples/meson-sdl"
ninja -v -C "$tmpdir"
"$tmpdir/hello"
ninja -v -C "$tmpdir" clean

echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
