#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that strace works.

echo "1..2"

strace="$(command -v strace)"
echo "ok 1 - strace exists at $strace"

if [ -e /.dockerenv ] || grep docker /proc/self/cgroup >/dev/null; then
    echo "ok 2 # SKIP - we cannot usually ptrace inside Docker"
    exit 0
fi

strace ls / >&2

echo "ok 2 - could use strace"

# vim:set sw=4 sts=4 et ft=sh:
