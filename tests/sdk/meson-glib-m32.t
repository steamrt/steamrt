#!/bin/bash
# Copyright © 2019-2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

case "$(dpkg --print-architecture)" in
    (amd64|i386)
        ;;
    (*)
        echo "1..0 # SKIP - not x86"
        exit 0
        ;;
esac

# Test that we can compile a simple project using meson and ninja.

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

# Meson wants a real locale
export LC_ALL=C.UTF-8

n=0
failed=

for cross in \
    gcc-m32.txt \
    gcc-4.6-m32.txt \
    gcc-4.8-m32.txt \
    gcc-5-m32.txt \
    gcc-9-m32.txt \
; do
    for aux in \
        '' \
        binutils-2.35.txt \
    ; do
        n=$((n + 1))

        if \
            meson setup \
                --cross-file "$cross" \
                ${aux:+--cross-file "$aux"} \
                "$tmpdir/b" \
                "$(pwd)/examples/meson-glib" \
            && ninja -v -C "$tmpdir/b" \
            && "$tmpdir/b/hello" \
            && ninja -v -C "$tmpdir/b" clean \
        ; then
            echo "ok $n - $cross ${aux:-default}"
        else
            echo "not ok $n - $cross ${aux:-default}"
            failed=1
        fi

        rm -fr "$tmpdir/b"
    done
done

echo "1..$n"

if [ -n "$failed" ]; then
    exit 1
fi

# vim:set sw=4 sts=4 et ft=sh:
