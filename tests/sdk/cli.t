#!/bin/bash
# Copyright © 2019-2020 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# shellcheck disable=SC1091
. /etc/os-release

failed=0
test_num=0

# These symlinks are set up by the container build, not by the packages
# we install, so we cannot rely on them existing in arbitrary minimal
# chroots.
case "${VARIANT_ID-}" in
    (com.valvesoftware.steamruntime.sdk-*)
        for c in \
            cpp \
            dos2unix \
            g++ \
            gcc \
            ifconfig \
            ip \
            nc \
            ping \
            ping6 \
            route \
            telnet \
            traceroute \
            traceroute6 \
            unix2dos \
            vi \
            xdg-email \
            xdg-open \
        ; do
            test_num=$(( test_num + 1 ))
            printf '# '
            if command -v "$c"; then
                echo "ok $test_num - $c found"
            else
                echo "$c not found"
                echo "not ok $test_num - $c not found"
                failed=1
            fi
        done
        ;;
    (*)
        echo '1..0 # SKIP not a Platform or SDK container'
        exit 0
        ;;
esac

echo "1..${test_num}"
exit "$failed"

# vim:set sw=4 sts=4 et ft=sh:
