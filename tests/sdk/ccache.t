#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple project using ccache.

echo "1..2"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

export CCACHE_DIR="$tmpdir/ccache"
ccache -s >&2

cp -a examples/make-glib/* "$tmpdir"

make -C "$tmpdir" V=1 CC='ccache cc'
"$tmpdir/hello"
make -C "$tmpdir" V=1 clean
ccache -s >&2

echo "ok 1"

export PATH="/usr/lib/ccache:$PATH"
make -C "$tmpdir" V=1
"$tmpdir/hello"
make -C "$tmpdir" V=1 clean
ccache -s >&2

echo "ok 2"

# vim:set sw=4 sts=4 et ft=sh:
