#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u

# This test doesn't really test anything non-trivial: it only verifies
# that the container is enough to run simple shell scripts.

echo "1..1"
printf 'not ok 1\n' | sed -e 's/not //g'

# vim:set sw=4 sts=4 et ft=sh:
