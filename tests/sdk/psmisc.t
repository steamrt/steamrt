#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u

echo "1..1"
pstree >&2
echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
