#!/bin/bash
# Copyright © 2019-2022 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile some simple C++ projects using make and g++-9.

# Also check that our binutils backport works
binutils_suffix="-2.35"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

CC=gcc-9
CXX=g++-9

i=0

for dir in cxx cxx2; do
    cp -a examples/"$dir"/* "$tmpdir"

    make -C "$tmpdir" V=1 CC="$CC" CXX="$CXX"
    "strip$binutils_suffix" "$tmpdir/hello"
    "objdump$binutils_suffix" -T -x "$tmpdir/hello" >&2
    "$tmpdir/hello"
    make -C "$tmpdir" V=1 CC="$CC" CXX="$CXX" clean

    i=$(( i + 1 ))
    echo "ok $i"
done

echo "1..$i"

# vim:set sw=4 sts=4 et ft=sh:
