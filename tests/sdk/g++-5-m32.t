#!/bin/bash
# Copyright © 2019 Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
# (See debian/copyright)

set -e
set -u
set -x

# Test that we can compile a simple C++ project using make and g++-5
# with the -m32 option.

case "$(dpkg --print-architecture)" in
    (amd64|i386)
        ;;
    (*)
        echo "1..0 # SKIP - not x86"
        exit 0
        ;;
esac

echo "1..1"

tmpdir="$(mktemp -d)"
cleanup () {
    rm -fr "$tmpdir"
}
trap cleanup 0 INT QUIT ABRT PIPE TERM

cp -a examples/cxx/* "$tmpdir"

CC="gcc-5 -m32"
CXX="g++-5 -m32"

make -C "$tmpdir" V=1 CC="$CC" CXX="$CXX"
file "$tmpdir/hello"
grep -F /lib/ld-linux.so.2 "$tmpdir/hello"
"$tmpdir/hello"
make -C "$tmpdir" V=1 CC="$CC" CXX="$CXX" clean

echo "ok 1"

# vim:set sw=4 sts=4 et ft=sh:
