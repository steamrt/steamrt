/*
 * Copyright © 2019 Collabora Ltd.
 * SPDX-License-Identifier: BSD-3-Clause
 * (See debian/copyright)
 */

#include <stdio.h>

int
main (int argc, char **argv)
{
    puts("Hello, world!");
    return 0;
}
