/*
 * Copyright © 2019 Collabora Ltd.
 * SPDX-License-Identifier: BSD-3-Clause
 * (See debian/copyright)
 */

#include <iostream>

using std::cout;

int
main (int argc, char **argv)
{
    cout << "Hello, world!\n";
    return 0;
}
