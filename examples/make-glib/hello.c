/*
 * Copyright © 2019 Collabora Ltd.
 * SPDX-License-Identifier: BSD-3-Clause
 * (See debian/copyright)
 */

#include <glib.h>

int
main (int argc, char **argv)
{
    g_message("Hello, world!");
    return 0;
}
