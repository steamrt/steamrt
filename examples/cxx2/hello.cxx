/*
 * Copyright © 2019-2022 Collabora Ltd.
 * Copyright © 2022 Valve Corporation
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * A slightly less trivial C++ program than examples/cxx.
 */

#include <iostream>
#include <mutex>

static std::once_flag flag;

int
main (int argc, char **argv)
{
    std::call_once(flag, []() {
        std::cout << "Hello, thread-safety!\n";
    });
    return 0;
}
